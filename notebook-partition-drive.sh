#!/bin/bash

install_device=`cat /tmp/maaniv/install-device`

parted -s "$install_device" \
        mklabel msdos \
        mkpart primary linux-swap 1MiB 8193MiB \
        mkpart primary ext2 8194MiB 9218MiB \
        mkpart primary ext4 9218MiB 100% \
        set 2 boot on
        
# swap partition
mkswap "${install_device}1"
swapon "${install_device}1"
# boot partition
mkfs.ext2 -F "${install_device}2"
# root partition
mkfs.ext4 -F "${install_device}3"

mount "${install_device}3" /mnt
mkdir /mnt/boot
mount "${install_device}2" /mnt/boot
