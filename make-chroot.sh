#!/bin/bash

pacstrap /mnt base base-devel bash-completion git
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt
