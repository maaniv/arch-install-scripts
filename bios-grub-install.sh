#!/bin/bash

install_device=`cat /tmp/maaniv/install-device`

pacman -S grub

grub-install --target=i386-pc ${install_device}
grub-mkconfig -o /boot/grub/grub.cfg
