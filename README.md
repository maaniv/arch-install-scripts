# arch-install-scripts

Scripts for install arch linux

Steps:

1. Check UEFI
    - ls /sys/firmware/efi/efivars
1. Check internet
    - ping archlinux.org
1. Wifi stuff
    1. ip link set <wlan0> up
    1. iwlist <wlan0> scan | grep ESSID
    1. wpa_supplicant -B -i <wlan0> -c <(wpa_passphrase MYSSID passphrase)
    1. systemctl stop dhcpcd@<wlan0>
    1. systemctl start dhcpcd@<wlan0>
1. Clone this repository
    1. pacman -Syy
    1. pacman -S git
    1. cd /tmp
    1. git clone https://gitlab.com/maaniv/arch-install-scripts.git
    1. cd arch-install-scripts
1. Update system clock
    - update-system-clock.sh
1. Show all available disks
    - fdisk -l | grep Disk
1. Set install device
    - set-install-device.sh /dev/sda
1. Partition drive
    - *-partition-drive.sh
1. Make chroot
    1. make-chroot.sh
    1. cd /tmp
    1. git clone https://gitlab.com/maaniv/arch-install-scripts.git
    1. cd arch-install-scripts
    1. set-install-device.sh /dev/sda
1. Install base packages
    - install.sh
1. Set video drivers
    - nvidia-390xx.sh ||
    - pacman -S nvidia ||
    - pacman -S xf86-video-*
1. Install grub
    - bios-grub-install.sh
    - uefi-grub-install.sh
1. Tweaks
    - udev-bfq.sh
    - sysctl-settings.sh
1. Done
    1. exit
    1. reboot
