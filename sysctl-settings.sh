#!/bin/bash

cat > /etc/sysctl.d/99-sysctl.conf <<EOF
kernel.sysrq = 1

# for vscode
fs.inotify.max_user_watches = 524288

vm.dirty_writeback_centisecs=100
vm.dirty_ratio=90
vm.dirty_background_ratio=5
vm.dirty_expire_centisecs=500
EOF
