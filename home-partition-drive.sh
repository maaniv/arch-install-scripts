#!/bin/bash

install_device=`cat /tmp/maaniv/install-device`

parted -s "$install_device" \
        mklabel gpt \
        mkpart primary fat32 1MiB 1025MiB \
        mkpart primary ext2 1026MiB 2050MiB \
        mkpart primary ext4 2051MiB 104451MiB \
        mkpart primary ext4 104451MiB 100% \
        set 2 boot on \
        set 1 esp on
        
# efi partition
mkfs.ext2 -F32 "${install_device}1"
# boot partition
mkfs.ext2 -F "${install_device}2"
# root partition
mkfs.ext4 -F "${install_device}3"
# home partition
mkfs.ext4 -F "${install_device}4"

mount "${install_device}3" /mnt
mkdir /mnt/boot
mount "${install_device}2" /mnt/boot
mkdir /mnt/home
mount "${install_device}4" /mnt/home
mkdir /mnt/efi
mount "${install_device}1" /mnt/efi
