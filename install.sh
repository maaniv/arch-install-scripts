#!/bin/bash

host_name=notebook-maaniv
user_name=maaniv

install_device=`cat /tmp/maaniv/install-device`

ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
hwclock --systohc

echo 'LANG=ru_RU.UTF-8' >> /etc/locale.conf
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen

echo "${host_name}" > /etc/hostname

cat > /etc/hosts <<EOF
127.0.0.1	localhost
::1		localhost
127.0.1.1	${host_name}.localdomain	${host_name}
EOF

#echo "127.0.0.1	localhost" >> /etc/hosts
#echo "::1		localhost" >> /etc/hosts
#echo "127.0.1.1	${host_name}.localdomain	${host_name}" >> /etc/hosts

passwd
#useradd -m -G adm,systemd-journal,wheel,rfkill,games,network,video,audio,optical,floppy,storage,scanner,power,adbusers,wireshark "$user_name"
useradd -m -G wheel,network,video,audio,storage,power "$user_name"
passwd "$user_name"

pacman -S sudo sddm plasma-meta kde-applications-meta

#git clone https://aur.archlinux.org/trizen.git
#cd trizen
#makepkg -si
#cd ..

#grub-install --target=i386-pc ${install_device}
#grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable sddm NetworkManager
